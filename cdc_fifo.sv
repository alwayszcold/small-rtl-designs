`ifndef __cdc__fifo__
`define __cdc__fifo__

module cdc_fifo #(
  parameter fifo_width_p          = 64,
  parameter fifo_depth_p          = 4 ,
  parameter use_external_memory_p = 0
) (
  // write clock domain for wr_en, wdata, full, wr_usedw
  input  logic                          clk_wr  ,
  input  logic                          rst_wr_n,
  input  logic                          wr_en   , // write enable
  input  logic [      fifo_width_p-1:0] wdata   , // write data
  output logic                          full    , // FIFO is full
  output logic [$clog2(fifo_depth_p):0] wr_usedw, // used FIFO slots at write clock domain
  // read clock domain for rd_en, rdata, empty, rd_usedw
  input  logic                          clk_rd  ,
  input  logic                          rst_rd_n,
  input  logic                          rd_en   , // read enable
  output logic [      fifo_width_p-1:0] rdata   , // read data
  output logic                          empty   , // FIFO is empty
  output logic [$clog2(fifo_depth_p):0] rd_usedw, // used FIFO slots at read clock domain
  // external memory interface, not used when use_external_memory_p == 1
  // use clk_wr
  output logic                            mem_wr_en, // write external memory when 1
  output logic [$clog2(fifo_depth_p)-1:0] mem_raddr, // read address for external memory
  output logic [$clog2(fifo_depth_p)-1:0] mem_waddr, // write address for external memory
  output logic [        fifo_width_p-1:0] mem_wdata, // write data to external memory
  input  logic [        fifo_width_p-1:0] mem_rdata  // read data from external memory
);

  localparam ptr_width_c = $clog2(fifo_depth_p);

  typedef logic [ptr_width_c-1:0] ptr_t;

  function ptr_t binary2gray(input ptr_t binary_coded);
    binary2gray = binary_coded ^ {1'b0, binary_coded[ptr_width_c-1:1]};
  endfunction : binary2gray

  function ptr_t gray2binary(input ptr_t gray_coded);
    gray2binary[ptr_width_c-1] = gray_coded[ptr_width_c-1];
    for(int i=ptr_width_c-2; i>=0; i--) begin
      gray2binary[i] = gray_coded[i] ^ gray2binary[i+1];
    end
  endfunction : gray2binary

  // read pointer signals
  ptr_t rptr_bin_rd     ; // binary read pointer in read domain
  ptr_t next_rptr_bin_rd; // next binary read pointer in read domain
  ptr_t rptr_rd         ; // Gray coded read pointer in read domain
  ptr_t rptr_rd_d1      ; // Gray coded read pointer in read domain, delayed one clock cycle
  ptr_t rptr_wr         ; // Gray coded read pointer in write domain
  ptr_t rptr_bin_wr     ; // binary read pointer in write domain

  // write pointer signals
  ptr_t wptr_bin_wr     ; // binary write pointer in write domain
  ptr_t next_wptr_bin_wr; // next binary write pointer in write domain
  ptr_t wptr_wr         ; // Gray coded write pointer in write domain
  ptr_t wptr_wr_d1      ; // Gray coded write pointer in write domain, delayed one clock cycle
  ptr_t wptr_rd         ; // Gray coded write pointer in read domain
  ptr_t wptr_bin_rd     ; // binary write pointer in read domain

  // sample read pointer from read domain to write domain
  always_ff @(posedge clk_wr or negedge rst_wr_n) begin : binary_rptr_to_wr_domain
    if (!rst_wr_n) begin
      rptr_wr    <= '0;
      rptr_rd_d1 <= '0;
    end else begin
      {rptr_wr, rptr_rd_d1} <= {rptr_rd_d1, rptr_rd};
    end
  end : binary_rptr_to_wr_domain

  // sample write pointer from write domain to read domain
  always_ff @(posedge clk_rd or negedge rst_rd_n) begin : binary_wptr_to_rd_domain
    if (!rst_rd_n) begin
      wptr_rd    <= '0;
      wptr_wr_d1 <= '0;
    end else begin
      {wptr_rd, wptr_wr_d1} <= {wptr_wr_d1, wptr_wr};
    end
  end : binary_wptr_to_rd_domain

  // address width for data storage
  localparam address_width_c = $clog2(fifo_depth_p);

  logic                       mem_wr_en_int; // output memory write enable
  logic [address_width_c-1:0] mem_raddr_int; // output memory read address
  logic [address_width_c-1:0] mem_waddr_int; // output memory write address
  logic [fifo_width_p-1:0]    mem_rdata_int; // input  memory read data in
  logic [fifo_width_p-1:0]    mem_wdata_int; // output memory write data out

  // fifo_memory
  generate
    if (use_external_memory_p) begin : use_external_memory
      assign mem_wr_en = mem_wr_en_int;
      assign mem_raddr = mem_raddr_int;
      assign mem_waddr = mem_waddr_int;
      assign mem_rdata_int = mem_rdata;
      assign mem_wdata = mem_wdata_int;
    end : use_external_memory
    else begin : internal_register_file
      logic [fifo_depth_p-1:0][fifo_width_p-1:0] register_file;
      assign mem_wr_en = '0;
      assign mem_raddr = '0;
      assign mem_waddr = '0;
      assign mem_wdata = '0;
      assign mem_rdata_int = register_file[mem_raddr_int];

      always_ff @(posedge clk_wr or negedge rst_wr_n) begin : proc_register_file
        if (!rst_wr_n) begin
          register_file <= '0;
        end else begin
          if (mem_wr_en_int) begin
            register_file[mem_waddr_int] <= mem_wdata_int;
          end
        end
      end : proc_register_file
    end : internal_register_file
  endgenerate

  assign mem_wr_en_int = wr_en;
  assign mem_waddr_int = wptr_bin_wr;
  assign mem_raddr_int = rptr_bin_rd;
  assign mem_wdata_int = wdata;
  assign rdata         = mem_rdata_int;

  // read domain controller -- generate rptr_rd, empty, rd_usedw
    always_comb begin : next_rptr
      // next read pointer is increased when
      //   read AND
      //   FIFO is not empty
      next_rptr_bin_rd = (rptr_bin_rd + (!empty && rd_en)) % fifo_depth_p;

      // binary to Gray conversion
      rptr_rd = binary2gray(rptr_bin_rd);
      wptr_bin_rd = gray2binary(wptr_rd);

      if (wptr_bin_rd > rptr_bin_rd) begin
        rd_usedw = wptr_bin_rd - rptr_bin_rd;
      end else begin
        case ({empty, wptr_bin_rd == rptr_bin_rd})
          2'b11: rd_usedw = '0;
          2'b01: rd_usedw = fifo_depth_p;
          default: rd_usedw = fifo_depth_p + wptr_bin_rd - rptr_bin_rd;
        endcase
      end
    end : next_rptr

    always_ff @(posedge clk_rd or negedge rst_rd_n) begin : rptr_counter
      if (!rst_rd_n) begin
        rptr_bin_rd <= '0;
        empty       <= '1;
      end else begin
        rptr_bin_rd <= next_rptr_bin_rd;
        empty       <= (next_rptr_bin_rd == wptr_bin_rd); // FIFO is empty when next read pointer == write pointer
        if (rd_en == 1'b1) begin
          read_when_empty : assert (!empty) else $error("FIFO read when empty.");
        end
      end
    end : rptr_counter

  // write domain controller -- generate wptr_wr, full, wr_usedw
    always_comb begin : next_wptr
      // next write pointer is increased when
      //   write AND
      //   FIFO is not full
      next_wptr_bin_wr = wptr_bin_wr + (wr_en && !full);

      // binary to Gray conversion
      wptr_wr = binary2gray(wptr_bin_wr);
      rptr_bin_wr = gray2binary(rptr_wr);

      if (wptr_bin_wr > rptr_bin_wr) begin
        wr_usedw = wptr_bin_wr - rptr_bin_wr;
      end else begin
        case ({full, wptr_bin_wr == rptr_bin_wr})
          2'b11: wr_usedw = fifo_depth_p;
          2'b01: wr_usedw = '0;
          default: wr_usedw = fifo_depth_p + wptr_bin_wr - rptr_bin_wr;
        endcase
      end
    end : next_wptr

    always_ff @(posedge clk_wr or negedge rst_wr_n) begin : wptr_counter
      if (!rst_wr_n) begin
        wptr_bin_wr <= '0;
        full        <= '0;
      end else begin
        wptr_bin_wr <= next_wptr_bin_wr;
        full        <= (next_wptr_bin_wr == rptr_bin_wr); // FIFO is full when next write pointer == read pointer
        if (wr_en == 1'b1) begin
          write_when_full : assert (!full) else $error("FIFO written when full.");
        end
      end
    end : wptr_counter

  endmodule

`endif
